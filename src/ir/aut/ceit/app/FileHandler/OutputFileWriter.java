package ir.aut.ceit.app.FileHandler;

import java.io.*;

/**
 * Created by Milad on 4/17/2017.
 */
public class OutputFileWriter {


    public void make(String inp, String type, String time) {
        String path =System.getProperty("user.dir") + "\\" + type + "\\" + time + ".txt" ;
        File f = new File(path);

        f.getParentFile().mkdirs();
        try {
            f.createNewFile();
            Writer output = new BufferedWriter(new FileWriter(f));
            output.write(inp);
            output.close();
        } catch (IOException e) {
        }

    }
}
