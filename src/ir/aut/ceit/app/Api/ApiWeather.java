package ir.aut.ceit.app.Api;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Milad on 4/15/2017.
 */
public class ApiWeather {

    /**
     * API_KEY is my api for site
     * address is the url for connection
     */
    private final String API_KEY = "&appid=35ee30ca79f24a42904054a6ddd74884";
    private String address = "http://api.openweathermap.org/data/2.5/";

    /**
     * get json from site
     * @return JSON string
     */
    public String getJSON() {
        String ret = "";
        try {
            URL urlWeather = new URL(address);
            HttpURLConnection httpURLConnection = (HttpURLConnection) urlWeather.openConnection();
            if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStreamReader inputStreamReader = new InputStreamReader(httpURLConnection.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader, 8192);
                String line = null;
                while ((line = bufferedReader.readLine()) != null) {
                    ret += line;
                }
                bufferedReader.close();
            } else {
                System.out.println("Error in httpURLConnection.getResponseCode()!!!");
            }
        } catch (MalformedURLException ex) {
            Logger.getLogger(ApiWeather.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ApiWeather.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException ex) {
            Logger.getLogger(ApiWeather.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ret;
    }

    /**
     * set the url for finding by city name
     * @param city is the name of city
     */
    public void setCurrent(String city) {
        address = address + "weather?" + "q=" + city + API_KEY;
    }

    /**
     * set the url for finding with coordinate
     * @param x is the lat coordinate
     * @param y is the lon coordinate
     */
    public void setCurrent(double x, double y) {
        address = address + "weather?" + "lat=" + x + "&lon=" + y + API_KEY;
    }

    /**
     * set the url for finding with index
     * @param index is the index of our city
     */
    public void setCurrent(int index) {
        address = address + "weather?" + "id=" + index + API_KEY;
    }

    /**
     * set the url for finding by city name
     * @param city is the name of city
     * @param cnt  is the number of days
     */
    public void setForecast(String city, int cnt) {
        address = address + "forecast/daily?q=" + city + "&cnt=" + cnt + API_KEY;
    }

    /**
     * @param x is the lat coordinate
     * @param y is the lon coordinate
     * @param cnt is the number of days
     */
    public void setForecast(double x, double y, int cnt) {
        address = address + "forecast/daily?lat=" + x + "&lon=" + y + "&cnt=" + cnt + API_KEY;
    }

    /**
     * set the url for finding with index
     * @param index is the index of city
     * @param cnt is the number of days
     */
    public void setForecast(int index, int cnt) {
        address = address + "forecast/daily?id=" + index + "&cnt=" + cnt + API_KEY;
    }
}
