package ir.aut.ceit.app;

import ir.aut.ceit.app.Api.ApiWeather;
import ir.aut.ceit.app.Weather.*;

import java.util.Scanner;

/**
 * Created by Milad on 4/15/2017.
 */
public class Main {

    public static void main(String[] args) {
        Scanner inp = new Scanner(System.in);
        while (true) {
            System.out.println("\nWhich one do you need?! \n 1.Current 2.Forecast \n Enter number of your choice\n");
            int type;
            String misc = "====================================================================================\nCity          Humidity          Description       Wind-Info        Coordination\n====================================================================================\n";
            if (inp.nextInt() == 1) {
                CurrentWeather cur = new CurrentWeather();
                System.out.println("Enter your choice\n 1-By City Name \n 2-By City Index \n 3-By City co'ordinate\n");
                type = inp.nextInt();
                if (type == 1) {
                    System.out.println("Enter the City Name\n");
                    cur.setCity(inp.next());
                } else if (type == 2) {
                    System.out.println("Enter the City Index\n");
                    cur.setIndex(inp.nextInt());
                } else {
                    System.out.println("Enter the coordinates\n");
                    cur.setCord(inp.nextDouble(), inp.nextDouble());
                }
                System.out.println(misc + cur.getResult());
            } else {
                ForecastWeather forecast = new ForecastWeather();
                System.out.println("Enter number of day to forecast\n");
                forecast.setCnt(inp.nextInt());
                System.out.println("Enter your choice\n 1-By City Name \n 2-By City Index \n 3-By City co'ordinate\n");
                type = inp.nextInt();
                if (type == 1) {
                    System.out.println("Enter the City Name\n");
                    forecast.setCity(inp.next());

                } else if (type == 2) {
                    System.out.println("Enter the City Index\n");
                    forecast.setIndex(inp.nextInt());
                } else {
                    System.out.println("Enter the coordinates\n");
                    forecast.setCord(inp.nextDouble(), inp.nextDouble());
                }
                System.out.println(misc + forecast.getResult());
            }
            System.out.println("Do you want to continue?! \n 1.YES 2.No");
            if (inp.nextInt() == 2)
                break;
        }
        System.out.println("GOODBYE!\n");
    }
}
