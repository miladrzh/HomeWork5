package ir.aut.ceit.app.Weather;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Milad on 4/15/2017.
 */
public class ForecastWeather extends BaseWeather {

    /**
     * is the number of days
     */
    public int cnt;

    /**
     * parse JSON
     * @return result of parse
     */
    public String getResult() {
        String ret = "";
        try {
            JSONObject baseJSON = new JSONObject(api.getJSON());
            JSONObject cityJSON = baseJSON.getJSONObject("city");
            String city = cityJSON.getString("name");
            double loncord = (cityJSON.getJSONObject("coord")).getDouble("lon");
            double latcord = (cityJSON.getJSONObject("coord")).getDouble("lat");

            JSONArray listJSON = baseJSON.getJSONArray("list");
            for (int i = 0; i < cnt; i++) {
                ret += (city + "         ");
                JSONObject dayJSON = listJSON.getJSONObject(i);
                JSONArray weatherJSONArray = dayJSON.getJSONArray("weather");
                JSONObject tempJSON = dayJSON.getJSONObject("temp");
                JSONObject weatherJSON = weatherJSONArray.getJSONObject(0);
                ret += (dayJSON.getDouble("humidity") + "            ");
                ret += (weatherJSON.getString("description") + "       ");
                ret += (dayJSON.getDouble("speed") + "       ");
                ret += "lat: " + latcord;
                ret += "  lon: " + loncord;
                ret += "\n====================================================================================\n";

            }
        } catch (JSONException J) {

        }
        write(ret, "forecast");
        return ret;
    }

    public void setCity(String city) {
        api.setForecast(city, cnt);
    }

    public void setCnt(int cnt) {
        this.cnt = cnt;
    }

    public void setIndex(int index) {
        api.setForecast(index, cnt);
    }

    public void setCord(double x, double y) {
        api.setForecast(x, y, cnt);
    }
}
