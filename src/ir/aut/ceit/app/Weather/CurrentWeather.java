package ir.aut.ceit.app.Weather;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Milad on 4/15/2017.
 */
public class CurrentWeather extends BaseWeather {

    /**
     * parse JSON
     * @return result of parse
     */
    public String getResult() {

        JSONObject baseJSON = new JSONObject(api.getJSON());
        String city = baseJSON.getString("name");
        JSONArray weatherJSON = baseJSON.getJSONArray("weather");
        JSONObject weather = weatherJSON.getJSONObject(0);
        String description = weather.getString("description");
        JSONObject mainJSON = baseJSON.getJSONObject("main");
        double humidity = mainJSON.getDouble("humidity");
        double loncord = (baseJSON.getJSONObject("coord")).getDouble("lon");
        double latcord = (baseJSON.getJSONObject("coord")).getDouble("lat");
        JSONObject windJSON = baseJSON.getJSONObject("wind");
        double windSpeed = windJSON.getDouble("speed");
        String ret = city + "       " + humidity + "       " + description + "       " + windSpeed + "       " + "lat: " + latcord + " lon: " + loncord;
        write(ret, "current");
        return ret;
    }

    /**
     * set city for api
     * @param city is the name of city
     */
    public void setCity(String city) {
        api.setCurrent(city);
    }

    /**
     * set index for api
     * @param index is the index of city
     */
    public void setIndex(int index) {
        api.setCurrent(index);
    }

    /**
     * set coordinate for api
     * @param x is the lat of city
     * @param y is the lon of city
     */
    public void setCord(double x, double y) {
        api.setCurrent(x, y);
    }
}
