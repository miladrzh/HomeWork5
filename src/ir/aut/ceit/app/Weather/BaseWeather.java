package ir.aut.ceit.app.Weather;

import ir.aut.ceit.app.Api.ApiWeather;
import ir.aut.ceit.app.FileHandler.OutputFileWriter;

/**
 * Created by Milad on 4/15/2017.
 */
public class BaseWeather {
    public ApiWeather api = new ApiWeather();

    /**
     * write result of query in file
     * @param inp is the string that we wanna write in file
     * @param type is type of query
     */
    public void write(String inp, String type) {
        OutputFileWriter oup = new OutputFileWriter();
        oup.make(inp, type, Long.toString(System.currentTimeMillis()));
    }


}
